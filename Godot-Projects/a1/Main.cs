using Godot;
using System;

public partial class Main : Node
{
	[Export]
	public PackedScene MobScene { get; set; }
	
	public override void _Ready()
	{
	}
	
	public void win()
	{
		var hud = GetNode<HUD>("HUD");
		hud.ShowMessage("You Won!");
		hud.ShowRestart();
	}

	
	public void GameOver()
	{
		GetNode<Timer>("MobTimer").Stop();
		GetNode<HUD>("HUD").ShowGameOver();
	}

	public void NewGame()
	{
		var player = GetNode<Player>("Player");
		var startPosition = GetNode<Marker2D>("StartPosition");
		player.Start(startPosition.Position);
		var hud = GetNode<HUD>("HUD");
		hud.ShowMessage("Get Ready!");
	}
}
