using Godot;
using System;

public partial class Lazer : Area2D
{
	int timerReset = 65;
	bool active = false;
	int timer = 0;
	
	public override void _Process(double delta)
	{
		timer = timer + 1;
		
		if (timer >= timerReset) {
			timer = 0;
			active = !active;
		}
		
		if (active) {
			Show();
			GetNode<CollisionShape2D>("CollisionShape2D").Disabled = false;
		} else {
			Hide();
			GetNode<CollisionShape2D>("CollisionShape2D").Disabled = true;
		}
	}
}

