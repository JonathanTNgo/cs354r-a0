using Godot;
using System;

public partial class HUD : CanvasLayer
{
	[Signal]
	public delegate void StartGameEventHandler();
	
	public void ShowMessage(string text)
	{
		var message = GetNode<Label>("Message");
		message.Text = text;
		message.Show();

		GetNode<Timer>("MessageTimer").Start();
	}
	
	public void ShowRestart()
	{
		var button = GetNode<Button>("StartButton");
		button.Text = "Restart";
		button.Show();
	}
	
	async public void ShowGameOver()
	{
		ShowMessage("Game Over");

		var messageTimer = GetNode<Timer>("MessageTimer");
		await ToSignal(messageTimer, Timer.SignalName.Timeout);

		var message = GetNode<Label>("Message");
		message.Text = "Make it to the star!\nDon't touch the lazers!\nW,S,D, - Movement\nSpace - Jump";
		message.Show();

		await ToSignal(GetTree().CreateTimer(1.0), SceneTreeTimer.SignalName.Timeout);
		GetNode<Button>("StartButton").Show();
		GetNode<ColorRect>("ColorRect").Show();
	}
	
	private void _on_message_timer_timeout()
	{
		GetNode<Label>("Message").Hide();
	}


	private void _on_button_pressed()
	{
		GetNode<ColorRect>("ColorRect").Hide();
		GetNode<Button>("StartButton").Hide();
		EmitSignal(SignalName.StartGame);
	}
}
