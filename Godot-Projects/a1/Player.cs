using Godot;
using System;

public partial class Player : CharacterBody2D
{
	[Signal]
	public delegate void HitEventHandler();
	
	public const float Speed = 200.0f;
	public const float JumpVelocity = -550.0f;
	// Get the gravity from the project settings to be synced with RigidBody nodes.
	public float gravity = ProjectSettings.GetSetting("physics/2d/default_gravity").AsSingle();

	public void Start(Vector2 position)
	{
		Position = position;
		Show();
	}

	public override void _PhysicsProcess(double delta)
	{
		Vector2 velocity = Velocity;

		// Add the gravity.
		if (!IsOnFloor())
			velocity.Y += gravity * (float)delta;

		// Handle Jump.
		if (Input.IsActionJustPressed("jump") && IsOnFloor())
			velocity.Y = JumpVelocity;

		// Get the input direction and handle the movement/deceleration.
		// As good practice, you should replace UI actions with custom gameplay actions.
		Vector2 direction = Input.GetVector("move_left", "move_right", "ui_up", "ui_down");
		var animatedSprite2D = GetNode<AnimatedSprite2D>("AnimatedSprite2D");

		if (direction != Vector2.Zero) {
			velocity.X = direction.X * Speed;
			animatedSprite2D.Play();
			animatedSprite2D.FlipV = false;
			// See the note below about boolean assignment.
			animatedSprite2D.FlipH = velocity.X < 0;
		} else {
			velocity.X = Mathf.MoveToward(Velocity.X, 0, Speed);
			animatedSprite2D.Stop();
		}
		
		Velocity = velocity;
		MoveAndSlide();
	}
	
	// When player touches lazer and dies, reload current scene
	private void die() {
		GetTree().ReloadCurrentScene();
	} 
	
	private void _on_star_detector_area_entered(Area2D area)
	{
		EmitSignal(SignalName.Hit);
	}
	
	private void _on_hazard_detector_area_entered(Area2D area)
	{
		GetTree().ReloadCurrentScene();
	}
}
